import styled from 'styled-components';
import * as theme from '../constants/theme';

export const Title = styled.h3`
    color: ${props => props.color || `${theme.colors.primary}`};
    font-size: ${props => `${props.fontSize}px` || `24px`};
    font-weight: ${props => props.fontWeight || `500`};
    text-align: ${props => props.textAlign || `left`};
    margin-bottom: ${props => `${props.marginBottom}px` || `24px`};
`

export const Paragraph = styled.p`
    color: ${props => props.color || `${theme.colors.primary}`};
    font-size: ${props => `${props.fontSize}px` || `14px`};
    font-weight: ${props => props.fontWeight || `400`};
`

export const Box = styled.div`
    display: flex;
    flex-direction: ${props => props.flexDirection || `column`};
    justify-content: ${props => props.justifyContent || `flex-end`};
    aling-items: ${props => props.alignItems || `center`};
`