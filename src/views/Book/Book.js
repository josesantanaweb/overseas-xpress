import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import 'font-awesome/css/font-awesome.min.css';
import './Book.scss';
import Header from './../../components/Header/Header';
import Footer from './../../components/Footer/Footer';
import SearhForm from './../../components/SearchForm/SearchForm';
import imgHotel from './../../image/miami.jpg';

class Book extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="container mt-5">
                    <SearhForm />
                    <div className="book">
                        <div className="filter">
                            <h3>400 Results</h3>
                            <div className="filter-map">
                                <div className="button button-primary">
                                    Show Map
                                </div>
                            </div>
                            <div className="filter-head">
                                <h4>Filter</h4>
                                <span>Remove All</span>
                            </div>
                            <div className="filter-start">
                                <h4>Start</h4>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="1"/>
                                        <label htmlFor="1"></label>
                                    </div>
                                    <div className="start">
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                    </div>
                                    <span className="filter-label">(26)</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="2"/>
                                        <label htmlFor="2"></label>
                                    </div>
                                    <div className="start">
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                    </div>
                                    <span className="filter-label">(27)</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="3"/>
                                        <label htmlFor="3"></label>
                                    </div>
                                    <div className="start">
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                    </div>
                                    <span className="filter-label">(23)</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="4"/>
                                        <label htmlFor="4"></label>
                                    </div>
                                    <div className="start">
                                        <i className="fa fa-star"></i>
                                        <i className="fa fa-star"></i>
                                    </div>
                                    <span className="filter-label">(23)</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="5"/>
                                        <label htmlFor="5"></label>
                                    </div>
                                    <div className="start">
                                        <i className="fa fa-star"></i>
                                    </div>
                                    <span className="filter-label">(23)</span>
                                </div>
                            </div>
                            <div className="filter-name">
                                <h4>Name Hotel</h4>
                                <div className="input-group">
                                    <input type="text" className="input" placeholder="Buscar"/>
                                </div>
                            </div>
                            <div className="filter-price">
                                <h4>Price for night</h4>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="6"/>
                                        <label htmlFor="6"></label>
                                    </div>
                                    <span className="filter-label">US($) 0 -74</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="7"/>
                                        <label htmlFor="7"></label>
                                    </div>
                                    <span className="filter-label">US($) 75 -149</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="6"/>
                                        <label htmlFor="6"></label>
                                    </div>
                                    <span className="filter-label">US($) 150 -170</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="7"/>
                                        <label htmlFor="7"></label>
                                    </div>
                                    <span className="filter-label">More US($) 200</span>
                                </div>
                            </div>
                            <div className="filter-services">
                                <h4>Services</h4>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="7"/>
                                        <label htmlFor="7"></label>
                                    </div>
                                    <span className="filter-label">Acceso a Internet</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="8"/>
                                        <label htmlFor="8"></label>
                                    </div>
                                    <span className="filter-label">Estacionamineto</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="9"/>
                                        <label htmlFor="9"></label>
                                    </div>
                                    <span className="filter-label">Aire Acondicionado</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="9"/>
                                        <label htmlFor="9"></label>
                                    </div>
                                    <span className="filter-label">Piscina</span>
                                </div>
                                <div className="filter-row">
                                    <div className="checkbox">
                                        <input type="checkbox" id="9"/>
                                        <label htmlFor="9"></label>
                                    </div>
                                    <span className="filter-label">Cerca de la playa</span>
                                </div>
                            </div>
                        </div>
                        
                        <div className="grid-items">
                           
                            { this.state.hotels.map((hotel, index) =>
                                
                                <div key={hotel.id} className="grid-item">
                                    <div className="book-image">
                                        <img src={imgHotel} alt="{imgHotel}"/>
                                    </div>
                                    <div className="book-content">
                                        <h3>{hotel.name}</h3>
                                        <div className="book-ranking">
                                            <i className="fa fa-star"></i>
                                            <i className="fa fa-star"></i>
                                            <i className="fa fa-star"></i>
                                        </div>
                                        <p className="price">From USD <span>${hotel.price}/night</span></p>
                                        <p className="taxes">Taxes y Feed not included</p>
                                        <div className="book-actions">
                                            <Link to='/details' className="button button-primary outline">Hotel Details</Link>
                                            <button className="button button-primary">Room y Rates</button>
                                        </div>
                                        <p className="unable">We were unable to find price</p>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        ) 
    }
    state = {
        hotels: [
            {
                id: 1,
                name: "Faena Miami Beach",
                category: 3,
                price: 343,
            },
            {
                id: 2,
                name: "Bitlmore",
                category: 4,
                price: 323,
            },
            {
                id: 3,
                name: "EAST, Miami",
                category: 4,
                price: 400,
            },
            {
                id: 4,
                name: "Eden Roc Miami Beach",
                category: 3,
                price: 233,
            },
            {
                id: 5,
                name: "Bitlmore",
                category: 4,
                price: 563,
            },
            {
                id: 6,
                name: "Stantard Miami",
                category: 4,
                price: 100,
            },
        ]
    }
}

export default Book;