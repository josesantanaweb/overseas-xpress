import React, { Component } from 'react';
import './Login.scss';
import logo from './../../image/logo.png';
import path from './../../image/path.svg';
import 'font-awesome/css/font-awesome.min.css';
class Login extends Component {
    render() {
        return (
            <div className="login">
                <div className="login-card">
                    <div className="login-path">
                        <img src={path} alt="{path}" />
                    </div>
                    <div className="login-brand">
                        <img src={logo} alt="{logo}" />
                    </div>
                    <div className="login-options">
                        <li className="is-active">Login</li>
                        <li>Sign Up</li>
                    </div>
                    <form className="login-form">
                        <div className="input-group">
                            <input className="input" placeholder="Email"/>
                        </div>
                        <div className="input-group">
                            <input className="input" placeholder="Password"/>
                            <i className="fa fa-eye input-icon"></i>
                        </div>
                        <div className="login-actions">
                            <span>Remember</span>
                            <span className="text-secondary">Forgot Password ?</span>
                        </div>
                        <div className="login-button">
                            <button className="button button-primary">Login</button>
                        </div>
                        <div className="login-orConnect">
                            <p>Or connect with</p>
                        </div>
                        <div className="login-social">
                            <i className="fa fa-facebook"></i>
                            <i className="fa fa-google"></i>
                        </div>
                    </form>
                </div>
            </div>
        ) 
    }
}

export default Login;