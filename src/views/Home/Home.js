import React, { Component } from 'react';
import Header from './../../components/Header/Header';
import Footer from './../../components/Footer/Footer';
import SearchForm from '../../components/SearchForm/SearchForm';
import Packages from '../../components/Packages/Packages';
import Destinations from '../../components/Destinations/Destinations';
import Reviews from '../../components/Reviews/Reviews';
import { StyleSheet, css } from 'aphrodite/no-important';
import * as theme from '../../constants/theme';

class Home extends Component {
	state = {
		hotels: [
			{
				id: 1,
				name: "Faena Miami Beach",
				category: 3,
				price: 343,
				city: "Paris",
				picture: "https://i.ibb.co/Xp78cBt/paris.jpg"
			},
			{
				id: 2,
				name: "Emirates",
				category: 4,
				price: 323,
				city: "Emirates",
				picture: "https://i.ibb.co/S3YMGhX/emirates.jpg"
			},
			{
				id: 3,
				name: "EAST, Miami",
				category: 4,
				price: 400,
				city: "Rome",
				picture: "https://i.ibb.co/HHyZ2wp/rome.jpg"
			},
			{
				id: 4,
				name: "Eden Roc Miami Beach",
				category: 3,
				price: 233,
				city: "Madrid",
				picture: "https://i.ibb.co/r34WxjR/madrid.jpg"
			},
			{
				id: 5,
				name: "Bitlmore",
				category: 4,
				price: 563,
				city: "London",
				picture: "https://i.ibb.co/JxZ390g/london.jpg"
			},
			{
				id: 6,
				name: "Stantard Miami",
				category: 4,
				price: 100,
				city: "Miami",
				picture: "https://i.ibb.co/h86h57J/miami.jpg"
			},
		],
		
		packages: [
			{
				id: 1,
				name: "Faena Miami Beach",
				category: 3,
				price: 343,
				city: "Paris",
				picture: "https://i.ibb.co/Xp78cBt/paris.jpg"
			},
			{
				id: 2,
				name: "Emirates",
				category: 4,
				price: 323,
				city: "Emirates",
				picture: "https://i.ibb.co/S3YMGhX/emirates.jpg"
			},
			{
				id: 3,
				name: "EAST, Miami",
				category: 4,
				price: 400,
				city: "Rome",
				picture: "https://i.ibb.co/HHyZ2wp/rome.jpg"
			},
			{
				id: 4,
				name: "Eden Roc Miami Beach",
				category: 3,
				price: 233,
				city: "Madrid",
				picture: "https://i.ibb.co/r34WxjR/madrid.jpg"
			},
		],
		reviews: [
			{
				id: 1,
				user: "https://randomuser.me/api/portraits/men/44.jpg",
				comment: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
			},
			{
				id: 2,
				user: "https://randomuser.me/api/portraits/men/45.jpg",
				comment: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam officia error omnis, similique rem beatae! Quam velit sed consectetur nobis tenetur similique",
			},
			{
				id: 3,
				user: "https://randomuser.me/api/portraits/men/46.jpg",
				comment: "Lorem ipsum dolor sit amet consectetur adipisicing elit.",
			},
		]
	}

	render() {
		return (
			<div>
				<Header />
				<main className="home">
					<div className={css(styles.homeMain)}>   
						<div className="container"> 
							<div className={css(styles.homeCaption)} data-aos="fade-right">
								<p>Reserve a simple hotel stay or elaborate a detailed itinerary</p>
								<h1 className="font-big">Welcome to OverseasXpress</h1>
								<button className="button button-primary">More Info</button>
							</div>
							<div className={css(styles.homeSearchForm) + " container"}>
								<SearchForm />
							</div>
						</div>
					</div>
					<div className={css(styles.homePackages)}>
						<div className="container">
							<Packages packages={this.state.packages} />
						</div>
					</div>
					<div className={css(styles.homeDestinations)}>
						<div className="container">
							<Destinations hotels={this.state.hotels} />
						</div>
					</div>
					<div className="homeReviews">
						<div className="container">
							<Reviews reviews={this.state.reviews}/>
						</div>
					</div>
				</main>
				<Footer />
			</div>
		) 
	}
}

// import background
let bgHome = require('../../image/bg-home.jpeg')
let bgHomePackages = require('../../image/packages-bg.jpeg')

const styles = StyleSheet.create({
	homeMain: {
		width: '100%',
    height: '400px',
    position: 'relative',
    padding: '4rem 0',
    display: 'flex',
    alignItems: 'center',
    background: `url(${bgHome})`,
    backgroundSize: 'cover',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center 100%',
		backgroundRepeat: 'no-repeat',
		':after': {
			content: '""',
			position: 'absolute',
			width: '100%',
			height: '100%',
			top: '0',
			left: '0',
			background: 'rgba(0, 0, 0, 0.2)',
			zIndex: '1'
		},
	},

	homeCaption: {
		marginBottom: '4rem',
    color: theme.colors.white,
    textAlign: 'center',
    position: 'relative',
    zIndex: '10'
	},

	homeSearchForm: {
		position: 'absolute',
    zIndex: 20,
    bottom: '-60px',
    left: '50%',
    transform: 'translate(-50%)'
	},

	homeDestinations: {
		background: `url(${bgHomePackages})`,
    height: 'auto',
    backgroundPosition: 'center 30%',
		backgroundRepeat: 'no-repeat',
		backgroundAttachment: 'fixed',
		backgroundSize: 'cover',
		position: 'relative',
		':after': {
			content: '""',
			width: '100%',
			height: '100%',
			background: 'rgba(255, 255, 255, 0.6)',
			position: 'absolute',
			zIndex: 1,
			top: 0
		}
	},
});


export default Home;