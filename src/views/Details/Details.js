import React from 'react';
import Header from './../../components/Header/Header';
import Footer from './../../components/Footer/Footer';
import { StyleSheet, css } from 'aphrodite/no-important';
import * as theme from '../../constants/theme';
import Slider from "react-slick";
let detailsSlider = require('../../image/bg-home.jpeg');
const Details = (props) => {
  var settings = {
    dots: false,
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 1,
    speed: 500
  };
  return (
    <div className={css(styles.details)}>
      <Header />
      <div className={css(styles.detailsHeader)}>
        <div className={css(styles.detailsContainer) + " container"}>
          <div>
            <div className={css(styles.detailsHotelName)}>
              <p className="font-xxlarge">Faena Miami Beach</p>
              <div className={css(styles.ranking)}>
                <i className="fa fa-star"></i>
                <i className="fa fa-star"></i>
                <i className="fa fa-star"></i>
              </div>
            </div>
            <p>1601 Collins Ave, Miami</p>
          </div>
          <div>
            <span className="button button-white outline">View<i className="fa fa-angle-down ml-3"></i></span>
          </div>
        </div>
      </div>
      <div className={css(styles.detailsNav) + " container"}>
        <span className={css(styles.detailsNavItem)}>Fotos</span>
        <span className={css(styles.detailsNavItem)}>Habitaciones</span>
        <span className={css(styles.detailsNavItem)}>Ubicacion</span>
        <span className={css(styles.detailsNavItem)}>Sobre el Hotels</span>
        <span className={css(styles.detailsNavItem)}>Opiniones</span>
      </div>

      <div className="container">
        <div className={css(styles.detailsPhotos)}>
          <Slider {...settings}>
            <img src={detailsSlider} alt={detailsSlider} className={css(styles.detailsPhotosItem)}/>
            <img src={detailsSlider} alt={detailsSlider} className={css(styles.detailsPhotosItem)}/>
            <img src={detailsSlider} alt={detailsSlider} className={css(styles.detailsPhotosItem)}/>
          </Slider>
        </div>
      </div>



      <div className={css(styles.detailsModify) + " container"}>
        <span className={css(styles.detailsModifyItem)}>Disponibilidad</span>
        <span className={css(styles.detailsModifyItem)}><i className="fa fa-calendar mr-4"></i>19 Abr - 25 Abr (9 Noches)</span>
        <span className={css(styles.detailsModifyItem)}><i className="fa fa-calendar mr-4"></i>2 Adultos, 1 Habitacion</span>
        <span className={css(styles.detailsModifyItem)}>Modificar <i className="fa fa-angle-down ml-3"></i></span>
      </div>

      <div className={css(styles.detailsRoom) + " container"}> 
        <div className={css(styles.detailsRoomHead)}>
          <p className="font-large text-primary">Habitacion Doble Economica</p>
          <p>Precio por Noche</p>
        </div>
        <div className={css(styles.detailsRoomRow)}>
          <div className="flex">
            <p className="mr-4">Solo Alojamiento</p>
            <p className="text-secondary">Cancelacion Gratis</p>
          </div>
          <div className="flex">
            <div className="mr-4">
              <p className="font-large text-primary">548$</p>
              <p className="text-secondary">Solo quedan 3</p>
            </div>
            <span className="button button-primary">Reservar</span>
          </div>
        </div>
      </div>

      <div className={css(styles.detailsRoom) + " container"}> 
        <div className={css(styles.detailsRoomHead)}>
          <p className="font-large text-primary">Suite 2 dormitorios Economica</p>
          <p>Precio por Noche</p>
        </div>
        <div className={css(styles.detailsRoomRow)}>
          <div className="flex">
            <p className="mr-4">Solo Alojamiento</p>
            <p className="text-secondary">Cancelacion Gratis</p>
          </div>
          <div className="flex">
            <div className="mr-4">
              <p className="font-large text-primary">5548$</p>
              <p className="text-secondary">Solo quedan 3</p>
            </div>
            <span className="button button-primary">Reservar</span>
          </div>
        </div>
      </div>

      <div className={css(styles.detailsRoomCard) + " container"}> 
        <div className={css(styles.detailsHotelName)}>
          <p className="font-xxlarge text-primary">Faena Miami Beach</p>
          <div className={css(styles.ranking)}>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
            <i className="fa fa-star"></i>
          </div>
        </div>
        <div className={css(styles.detailsRoomDescription)}>
          <p className="font-large text-primary mb-3">Descripcion</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare ornare tortor, a vulputate risus. Duis dictum, ante ac ultrices facilisis, nulla nulla tempus lectus, vel volutpat mi nisl pellentesque metus. Nunc nec dui sit amet ex malesuada imperdiet. Suspendisse efficitur nulla lacus, non ultrices diam sagittis at. Aliquam erat volutpat. Suspendisse potenti. Pellentesque eu purus commodo, ornare mauris sed, aliquet lorem. Nunc luctus et tellus nec ullamcorper. Vestibulum ante orci, viverra a ultricies vitae, pulvinar id orci. Sed eget felis pulvinar, gravida erat vel, posuere risus. Vestibulum eu sem a neque tempus sollicitudin tristique non libero. Aenean quis elit a ipsum dapibus pharetra molestie vel lacus.</p>
        </div>
        <div className={css(styles.detailsRoomCheck)}>
          <p className="font-large text-primary mb-3">Check-in</p>
          <div className="flex mb-2">
            <p className="mr-2">
              <i className="fa fa-calendar text-primary mr-2"></i>    
              Check-in
            </p>
            <p>From 16: 00 hours</p>
          </div>
          <div className="flex mb-2">
            <p className="mr-2">
              <i className="fa fa-calendar text-primary mr-2"></i>    
              Check-out
            </p>
            <p>From 11: 00 hours</p>
          </div>
        </div>

        <div className={css(styles.detailsRoomServices)}>
          <p className="font-large text-primary mb-3">Servicios del Hotels</p>
          <div className={css(styles.detailsRoomServicesItems)}>
            <p className="mb-3">
              <i className="fa fa fa-cutlery text-primary mr-2"></i>
              Alimentos y Bebidas
            </p>
            <p className="mb-3">
              <i className="fa fa-shower text-primary mr-2"></i>
              Piscinas
            </p>
            <p className="mb-3">
              <i className="fa fa-shopping-bag text-primary mr-2"></i>
              Tiendas
            </p>
            <p className="mb-3">
              <i className="fa fa-music text-primary mr-2"></i>
              Disco
            </p>
          </div>
        </div>

        <div className={css(styles.detailsRoomServices)}>
          <p className="font-large text-primary mb-3">Servicios de la Habitacion</p>
          <div className={css(styles.detailsRoomServicesItems)}>
            <p className="mb-3">
              <i className="fa fa fa-cutlery text-primary mr-2"></i>
              Alimentos y Bebidas
            </p>
            <p className="mb-3">
              <i className="fa fa-shower text-primary mr-2"></i>
              Piscinas
            </p>
            <p className="mb-3">
              <i className="fa fa-shopping-bag text-primary mr-2"></i>
              Tiendas
            </p>
            <p className="mb-3">
              <i className="fa fa-music text-primary mr-2"></i>
              Disco
            </p>
          </div>
        </div>
        
        <div className={css(styles.detailsRoomInfo)}>
          <p className="font-large text-primary mb-3">Informacion util</p>
          <p className="mb-3">Como llegar</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare ornare tortor, a vulputate risus. Duis dictum, ante ac ultrices facilisis, nulla nulla tempus lectus, vel volutpat mi nisl pellentesque metus. Nunc nec dui sit amet ex malesuada imperdiet. Suspendisse efficitur nulla lacus, non ultrices diam sagittis at. Aliquam erat volutpat. Suspendisse potenti. Pellentesque eu purus commodo, ornare mauris sed, aliquet lorem. Nunc luctus et tellus nec ullamcorper. Vestibulum ante orci, viverra a ultricies vitae, pulvinar id orci. Sed eget felis pulvinar, gravida erat vel, posuere risus. Vestibulum eu sem a neque tempus sollicitudin tristique non libero. Aenean quis elit a ipsum dapibus pharetra molestie vel lacus.</p>
        </div>
      </div>

      <div className={css(styles.detailsRoomCard) + " container"}> 
        <p className="font-large text-primary mb-3">Opiniones Recientes</p>
        <div className={css(styles.detailsReview)}>   
          <div className={css(styles.detailsReviewUser)}> 
            <p className={css(styles.detailsReviewUserQly)}>8.5</p>
            <img 
              src={'https://randomuser.me/api/portraits/men/44.jpg'} 
              className={css(styles.detailsReviewUserImg)}/>
            <p className="text-primary">Mario Cepeda</p>
          </div> 
          <div className={css(styles.detailsReviewText)}> 
            <p className="font-medium mb-3">Excelente lugar</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare ornare tortor, a vulputate risus. Duis dictum, ante ac ultrices facilisis, nulla nulla tempus lectus, vel volutpat mi nisl pellentesque metus. Nunc nec dui sit amet ex malesuada imperdiet. Suspendisse efficitur nulla lacus, non ultrices diam sagittis at. Aliquam erat volutpat. Suspendisse potenti. Pellentesque eu purus commodo, ornare mauris sed, aliquet lorem. Nunc luctus et tellus nec ullamcorper. Vestibulum ante orci, viverra a ultricies vitae, pulvinar id orci. Sed eget felis pulvinar, gravida erat vel, posuere risus. Vestibulum eu sem a neque tempus sollicitudin tristique non libero. Aenean quis elit a ipsum dapibus pharetra molestie vel lacus.</p>
          </div> 
        </div> 
        <div className={css(styles.detailsReview)}>   
          <div className={css(styles.detailsReviewUser)}> 
            <p className={css(styles.detailsReviewUserQly)}>8.5</p>
            <img 
              src={'https://randomuser.me/api/portraits/men/45.jpg'} 
              className={css(styles.detailsReviewUserImg)}/>
            <p className="text-primary">Jose Torres</p>
          </div> 
          <div className={css(styles.detailsReviewText)}> 
            <p className="font-medium mb-3">Refaccion</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ornare ornare tortor, a vulputate risus. Duis dictum, ante ac ultrices facilisis, nulla nulla tempus lectus, vel volutpat mi nisl pellentesque metus. Nunc nec dui sit amet ex malesuada imperdiet. Suspendisse efficitur nulla lacus, non ultrices diam sagittis at. Aliquam erat volutpat. Suspendisse potenti. Pellentesque eu purus commodo, ornare mauris sed, aliquet lorem. Nunc luctus et tellus nec ullamcorper. Vestibulum ante orci, viverra a ultricies vitae, pulvinar id orci. Sed eget felis pulvinar, gravida erat vel, posuere risus. Vestibulum eu sem a neque tempus sollicitudin tristique non libero. Aenean quis elit a ipsum dapibus pharetra molestie vel lacus.</p>
          </div> 
        </div> 
        <div className={css(styles.detailsReview)}>   
          <div className={css(styles.detailsReviewUser)}> 
            <p className={css(styles.detailsReviewUserQly)}>8.5</p>
            <img 
              src={'https://randomuser.me/api/portraits/men/46.jpg'} 
              className={css(styles.detailsReviewUserImg)}/>
            <p className="text-primary">Mario Nieto</p>
          </div> 
        </div> 
      </div> 

      <div className={css(styles.detailsReviewReady)}> 
        <p className="font-xlarge mr-5">Listo para reservar?</p>
        <span className="button button-white outline">Mostrar Habitaciones</span>
      </div> 
      <Footer />
    </div> 
  ) 
}

const styles = StyleSheet.create({
	details: {
    background: theme.colors.white
  },
	detailsHeader: {
    background: theme.colors.primary,
    color: theme.colors.white,
    padding: '5px 0'
  },
	detailsContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
	detailsHotelName: {
    display: 'flex',
    alignItems: 'center'
  },
	ranking: {
    display: 'flex',
    alignItems: 'center',
    margin: '0 10px',
    color: theme.colors.secondary,
    fontSize: '18px',
    letterSpacing: '5px'
  },
  detailsNav: {
    display: 'flex',
    alignItems: 'center',
    borderBottom: `1px solid ${theme.colors.grayLight}`
  },
  detailsNavItem: {
    display: 'flex',
    padding: '10px',
    fontSize: '14px',
    fontWeight: '400',
    cursor: 'pointer',
    ':hover': {
      color: theme.colors.primary,
		},
  },
  detailsPhotos: {
    width: '100%',
    height: '450px',
  },
  detailsPhotosItem: {
    width: '100%',
    height: '450px',
  },
  detailsModify: {
    display: 'flex',
    alignItems: 'center',
    boxShadow: theme.boxShadow.base,
    margin: '30px auto',
    background: theme.colors.white
  },
  detailsModifyItem: {
    color: theme.colors.primary,
    display: 'flex',
    alignItems: 'center',
    padding: '15px',
    fontSize: '14px',
    fontWeight: '400',
    cursor: 'pointer',
  },
  detailsRoom: {
    fontSize: '14px',
    boxShadow: theme.boxShadow.base,
    margin: '30px auto',
    background: theme.colors.white
  },
  detailsRoomHead: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottom: `1px solid ${theme.colors.grayLight}`,
    padding: '10px'
  },
  detailsRoomRow: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '10px'
  },
  detailsRoomCard: {
    padding: '20px',
    boxShadow: theme.boxShadow.base,
    margin: '30px auto',
    background: theme.colors.white
  },
  detailsRoomDescription: {
    borderBottom: `1px solid ${theme.colors.grayLight}`,
    padding: '30px 0',
  },
  detailsRoomCheck: {
    borderBottom: `1px solid ${theme.colors.grayLight}`,
    padding: '30px 0',
  },
  detailsRoomServices: {
    padding: '30px 0',
    borderBottom: `1px solid ${theme.colors.grayLight}`
  },
  detailsRoomServicesItems: {
    display: 'grid',
    gridTemplateColumns: '200px 200px'
  },
  detailsRoomInfo: {
    padding: '30px 0',
  },
  detailsReview: {
    display: 'grid',
    gridTemplateColumns: '150px auto',
    padding: '30px 0',
    borderBottom: `1px solid ${theme.colors.grayLight}`,
  },
  detailsReviewUser: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    position: 'relative'
  },
  detailsReviewUserImg: {
    width: '80px',
    height: '80px',
    borderRadius: '80px',
    marginBottom: '10px',
    border: `2px solid ${theme.colors.secondary}`,
  },
  detailsReviewUserQly: {
    width: '30px',
    height: '30px',
    borderRadius: '30px',
    position: 'absolute',
    top: '0',
    right: '20px',
    background: theme.colors.primary,
    color: theme.colors.white,
    textAlign: 'center',
    lineHeight: '30px'
  },
  detailsReviewText: {
    marginLeft: '30px',
  },
  detailsReviewReady: {
    background: theme.colors.primary,
    padding: '30px 0',
    color: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '4rem 0 14rem'
  },
});

export default Details;