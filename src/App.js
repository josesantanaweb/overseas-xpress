import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import AOS from 'aos';
import 'aos/dist/aos.css';
import './App.scss';
import Home from './views/Home/Home';
import Login from './views/Login/Login';
import Book from './views/Book/Book';
import Details from './views/Details/Details';

class App extends Component {
  componentDidMount(){
    AOS.init({
      duration : 1000
    })
  }
  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/login' component={Login}/>
          <Route path='/book' component={Book}/>
          <Route path='/details' component={Details}/>
        </Switch>
      </div>
    );
  }
}

export default App;
