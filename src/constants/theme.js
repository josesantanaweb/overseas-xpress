


const colors = {
	primary: '#00A37C',
	secondary: '#FA8220',
	white: '#FFFFFF',
	black: '#000000',
	gray: '#B9C5D8',
	grayLight: '#F0F3F8',
	grayDark: '#A6B1C2'
}

const sizes = {
	base: 1,
	h1: 3.2,
	h2: 3,
	h3: 2.8,
	h4: 2.4,
	h5: 2.2,
	h6: 1.8,
}

const fonts = {
	h1: sizes.h1,
	h2: sizes.h2,
	h3: sizes.h3,
	h4: sizes.h4,
	h5: sizes.h5,
	h6: sizes.h6,
	body: sizes.base,
}

const border = {
	base: .1,
}

const borderRadius = {
	base: 5,
}

const boxShadow = {
  base: '0 5px 30px rgba(0, 0, 0, 0.1)'
}

const breackpoints = {
	xsmall: '@media (min-width: 0)',
	small: '@media (min-width: 576px)',
	medium: '@media (min-width: 768px)',
	large: '@media (min-width: 992px)',
	xlarge: '@media (min-width: 1300px)',
	xxlarge: '@media (min-width: 1400px)',
	xxxlarge: '@media (min-width: 1600px)'
}

export { colors, border, boxShadow, borderRadius, sizes, fonts, breackpoints }