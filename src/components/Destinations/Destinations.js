import React from 'react';
import Destiny from './Destiny';
import { StyleSheet, css } from 'aphrodite/no-important';
import * as theme from './../../constants/theme';
import { Title, Box } from '../../styles/styles'

const Destinations = (props) => {
	return(
		<Box className={css(styles.destinations)}>
			<Title 
				fontSize={24} 
				fontWeight={500} 
				marginBottom={40}
				textAlign={'center'}>
				Most Favorite Destinations
			</Title>
			<Box className={css(styles.destinationsItems)}>
				{ props.hotels.map((hotel, index) =>
					<Destiny
						key={hotel.id}
						city={hotel.city}
						picture={hotel.picture}
					/>
				)}
			</Box>
		</Box>
	) 
}

const styles = StyleSheet.create({
	destinations: {
		width: '100%',
		position: 'relative',
		zIndex: 10,
		padding: '5rem 0',
	},
	
	destinationsItems: {
		display: 'grid',
		gridTemplateColumns: 'repeat(4, 1fr)',
		gridTemplateRows: 'repeat(2, 250px)',
		gridGap: '2rem',
		color: theme.colors.white
	}
	
});

export default Destinations;