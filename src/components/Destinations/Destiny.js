import React from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';
import * as theme from './../../constants/theme';

const Destiny = (props) => {
	return (
		<div className={css(styles.destinationsItem)} key={props.id} data-aos="fade-down">
			<img src={props.picture} alt={props.picture} className={css(styles.destinationsItemImg)} />
			<div className={css(styles.destinationsLabel)}>
				<h4 className="font-large">{props.city}</h4>
			</div>
		</div>
	) 
}

const styles = StyleSheet.create({
	destinationsItem: {
		borderRadius: '5px',
		overflow: 'hidden',
		cursor: 'pointer',
		position: 'relative',
		boxShadow: theme.boxShadow.base,
		transition: 'all .2s',
		':last-child': {
			gridColumnStart: 1,
			gridRowStart: 2,
			gridColumnEnd: 3,
			gridRowEnd: 2
		},
	},
	destinationsItemImg: {
		height: '100%',
    objectFit: 'cover',
		objectPosition: 'center',
		transition: 'all .2s',
		':hover': {
			boxShadow: theme.boxShadow.base
		},
	},
	destinationsLabel: {
		position: 'absolute',
    top: '47%',
    left: '50%',
    transform: 'translate(-50%)'
	}
});

export default Destiny;