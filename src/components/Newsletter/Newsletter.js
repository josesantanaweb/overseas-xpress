import React from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';
import * as theme from '../../constants/theme';

const Newsletter = (props) => {
  return (
    <div className={css(styles.newsletter) + " container"}>
      <div>
        <p className="text-primary font-xxlarge">Susbcribe to our newsletter</p>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing.</p>
      </div>
      <div className="input-group">
        <input type="text" className="input" placeholder="Your email address"/>
        <button className="button button-primary">Send</button>
      </div>
    </div> 
  ) 
}

const styles = StyleSheet.create({
	newsletter: {
    background: theme.colors.white,
    boxShadow: '0 3px 30px rgba(0, 0, 0, 0.1)',
    width: '100%',
    padding:' 4rem 5rem !important',
    position: 'absolute',
    top: '-100px',
    zIndex: 10,
    borderRadius: theme.borderRadius.base,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
});

export default Newsletter;