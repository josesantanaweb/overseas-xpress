import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { StyleSheet, css } from 'aphrodite/no-important';
import * as theme from '../../constants/theme';
import logo from './../../image/logo.png';

class Header extends Component {
	render() {
		return (
			<header className={css(styles.header) + " container"}>
				<div className={css(styles.navbar)}>
					<Link to='/' className={css(styles.logo)}>
						<img src={logo} alt="{logo}" />
					</Link>
					<nav className={css(styles.menu, styles.menuLeft)}>
						<li>
							<Link to='/book' className={css(styles.menuLink)}>Hotels</Link>
						</li>
						<li>
							<a href="/" className={css(styles.menuLink)}>Experiences</a>
						</li>
						<li>
							<a href="/" className={css(styles.menuLink)}>Airport Transfer</a>
						</li>
					</nav>
					<div className={css(styles.menu, styles.menuRight)}>
						<li>
							<Link to='/login' className={css(styles.menuLink, styles.menuLink_isOutline)}>Sign In</Link>
						</li>
						<li>
							<Link to='/login' className={css(styles.menuLink)}>Sign Up</Link>
						</li>
					</div>
				</div>
			</header>  
		) 
  }
}

const styles = StyleSheet.create({
	header: {
		background: theme.colors.white,
		borderBottom: `1px solid ${theme.colors.grayLight}`
	},

	navbar: {
		display: 'flex',
		alignItems: 'center'
	},

	logo: {
		width: '80px'
	},

	menu: {
		width: '90px',
		display: 'flex',
		'align-items': 'center'
	},

	menuRight: {
		flex: 1,
		display: 'flex',
		justifyContent: 'flex-end'
	},

	menuLeft: {
		flex: 2,
		marginLeft: '5rem'
	},

	menuLink: {
		color: theme.colors.gray,
		padding: '.5rem 1rem',
		fontSize: '1.4rem',
		display: 'block',
		margin: '0 1rem;',
		borderRadius: '2px',
		':hover': {
			color: theme.colors.primary
		}
	},

	menuLink_isOutline: {
		border: '1px solid',
		borderColor: theme.colors.gray,
		':hover': {
			background: theme.colors.primary,
			color:  theme.colors.white,
			borderColor: theme.colors.primary
		}
	},
});

export default Header;