import React from'react';
import Package from './Package';
import Title from './../Title/Title';
import { StyleSheet, css } from 'aphrodite/no-important';

const Packages = (props) => {
	return(
		<div className={css(styles.packages)}>
			<Title label="Most Popular Packages" />   
			<div className={css(styles.packagesItems)}>
				{ props.packages.map((pack, index) =>
					<Package
						key={pack.id}
						city={pack.city}
						picture={pack.picture}
						category={pack.category}
						price={pack.price}
					/>
				)}	
			</div>            
		</div>
	) 
}

const styles = StyleSheet.create({
	packages: {
		width: '100%',
    position: 'relative',
    zIndex: 10,
    padding: '10rem 0 5rem 0'
	},

	packagesItems: {
		display: 'grid',
    gridTemplateColumns: 'repeat(4, 1fr)',
		gridGap: '3rem'
	},
});


export default Packages;