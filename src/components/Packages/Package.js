import React from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';
import * as theme from '../../constants/theme';

const Package = (props) => {
	return (
    <div className={css(styles.packagesItem)} key={props.id}>
      <div className={css(styles.packagesSelect)}>
        <span className="button button-primary">More Info</span>
      </div>
      <img className={css(styles.packagesItemImg)}  src={props.picture} alt={props.picture}/>
      <div className={css(styles.packagesContent)}>
        <li>
          <p>Guide Tour</p>
          <p className="text-primary font-medium">{props.city}</p>
          <span>
            { [...Array(props.category)].map((e, i) =>
              <i className="fa fa-star text-secondary font-medium" key={i}></i>
            )}
          </span>
        </li>
        <li>
          <p className="text-primary">{props.price}$ / night</p>
          <p className="">7 days tour</p>
        </li>
     </div>
    </div>
	) 
}

const styles = StyleSheet.create({
	packagesItem: {
    height: '324px',
    position: 'relative',
    cursor: 'pointer',
    '@media (min-width: 1400px)': {
      height: '550px',
		},
  },
  
	packagesItemImg: {
    boxShadow: theme.boxShadow.base,
    width: '100%',
    height: '100%',
    objectFit: 'cover',
    position: 'relative',
    zIndex: 2,
    transition: 'all 300ms',
    borderRadius: theme.borderRadius.base,
    ':hover': {
      transform: 'translateY(-70px)'
		}
	},

	packagesSelect: {
		position: 'absolute',
    top: '47%',
    left: '50%',
    zIndex: 3,
    transform: 'translate(-50%)',
    opacity: 0,
    visibility: 'hidden',
    transition: 'all .2s'
  },
  
	packagesContent: {
    width: '100%',
    position: 'absolute',
    bottom: '30px',
    left: 0,
    zIndex: 1,
    height: '30px',
    color: theme.colors.black,
    display: 'flex',
    justifyContent: 'space-between'
	},
});


export default Package;