import React from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';
import Review from './Review';
import Title from './../Title/Title'

const Reviews = (props) => {
	return(
		<div className={css(styles.reviews)}>
			<Title label="Travelers Experience"/>
			<div className={css(styles.reviewsItems)}>
				{ props.reviews.map((review, index) =>
					<Review
						key={review.id}
						user={review.user}
						comment={review.comment}
					/>
				)}
			</div>
		</div>
	) 
}

const styles = StyleSheet.create({
	reviews: {
		padding: '5rem 0',
		marginBottom: '10rem',
		width: '100%',
		position: 'relative',
		zindex: '10'
	},
	
  reviewsItems: {
		display: 'grid',
		gridTemplateColumns: '1fr 2fr 1fr',
		gridGap: '2rem',
		alignItems: 'flex-end',
		padding: '2rem 0'
	},
});

export default Reviews;