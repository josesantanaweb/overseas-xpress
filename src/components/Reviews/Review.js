import React from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';
import * as theme from '../../constants/theme';

const Review = (props) => {
	return (
		<div className="reviews-item" key={props.id} data-aos="fade-right">
			<div className={css(styles.reviewsUser)}>
				<img src={props.user} alt="{props.user}" className={css(styles.reviewsUserImg)}/>
			</div>
			<div className={css(styles.reviewsComment)}>
        <i className={css(styles.reviewsQuotes) + " fa fa-quote-left"}></i>
				<span>{props.comment}</span>
        <i className={css(styles.reviewsQuotes) + " fa fa-quote-right"}></i>
			</div>
		</div>
	) 
}

const styles = StyleSheet.create({
	reviewsUser: {
		width: '80px',
    borderRadius: '80px',
    height: '80px',
    overflow: 'hidden',
    border: `2px solid ${theme.colors.secondary}`,
    margin: 'auto',
    display: 'block',
    marginBottom: '2rem'
	},

	reviewsUserImg: {
		objectFit: 'cover',
		objectPosition: 'center',
	},

	reviewsComment: {
		width: '100%',
    borderRadius: '10px',
    background: theme.colors.white,
    fontSize: '16px',
    padding: '4rem 3rem',
    boxShadow: theme.boxShadow.base,
		position: 'relative',
	},

	reviewsQuotes: {
		position: 'absolute',
		fontSize: '18px !important',
		':first-child': {
			top: '10px',
			left: '10px'
		},
		':last-child': {
			bottom: '10px',
			right: '10px'
		}
	},
});


export default Review;