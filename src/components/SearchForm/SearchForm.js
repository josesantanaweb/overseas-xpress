import React, { Component } from 'react';
import './SearchForm.scss';
import { Link } from 'react-router-dom';
class SearchForm extends Component {
    render() {
        return (
            <div className="search">
                <div className="search-tabs">
                    <li className="is-active">Hotels</li>
                    <li>Experiences</li>
                    <li>Airport Transfer</li>
                </div>
                <div className="search-body">
                    <div className="search-form">
                        <div className="input-group">
                            <label className="label">Location</label>
                            <input type="text" className="input" placeholder="Where are you planning to travel?"/>
                        </div>
                        <div className="input-group">
                            <label className="label">Check In / Check Out</label>
                            <input type="text" className="input" placeholder="Check In / Check Out"/>
                        </div>
                        <div className="input-group">
                            <label className="label">Occupancy</label>
                            <input type="text" className="input" placeholder="Occupancy"/>
                        </div>
                        <div className="input-group">
                            <Link to='/book' className="button button-primary">Search</Link>
                        </div>
                    </div>
                </div>
            </div>
        ) 
    }
}

export default SearchForm;