import React from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';
import * as theme from '../../constants/theme';

const Title = (props) => {
	return(
		<h3 className={css(styles.title)}>{ props.label }</h3>
	) 
}

const styles = StyleSheet.create({
  title: {
		color: theme.colors.primary,
		position: 'relative',
		textAlign: 'center',
		fontSize: `${theme.fonts.h3}rem`,
		fontWeight: 500,
		margin: 'auto', 
		marginBottom: '6rem'
	},
});


export default Title