import React from 'react';
import Newsletter from '../Newsletter/Newsletter';
import { StyleSheet, css } from 'aphrodite/no-important';
import * as theme from '../../constants/theme';

const Footer = (props) => {
	return (
		<div className={css(styles.footer)}>
			<div className="container">
					<Newsletter />
					<div className={css(styles.footerItems)}>
						<div className="footer-item">
							<li className={css(styles.footerItemList)}>Currency</li>
							<input type="text" className="input" placeholder="USD United State Dollar ($)"/>
						</div>
						<div className="footer-item">
							<li className={css(styles.footerItemList)}>Contact</li>
							<li className={css(styles.footerItemList)}>Miami Office: +1.786.276.8686</li>
							<li className={css(styles.footerItemList)}>UK Office: +44.203.608.4344</li>
							<li className={css(styles.footerItemList)}>FR Office: +33.182.882.736</li>
							<li className={css(styles.footerItemList)}>814 Ponce de Leon Blvd #400, Coral Gables, FL 33134</li>
							<li className={css(styles.footerItemList)}>ox@overseasinternational.com</li>
						</div>
						<div className="footer-item">
							<li className={css(styles.footerItemList)}>Nav</li>
							<li className={css(styles.footerItemList)}>Hotels</li>
							<li className={css(styles.footerItemList)}>Experiences</li>
							<li className={css(styles.footerItemList)}>Airport Transfer</li>
						</div>
					</div>
					<div className={css(styles.footerCopy)}>
						<li className={css(styles.footerItemList)}>Overseas Travel of Florida | All Rights Reserved </li>
						<li className={css(styles.footerItemList)}>Terms & Privacy Policy</li>
					</div>
			</div>
		</div>
	) 
}

// import background
let bgFooter = require('../../image/footer-bg.jpeg')


const styles = StyleSheet.create({
	footer: {
    width: '100%',
    padding: '10rem 0 2rem 0',
    background: `url(${bgFooter})`,
    backgroundPosition: '60% 80%',
    backgroundRepeat: 'no-repeat',
    position: 'relative',
    backgroundAttachment: 'fixed',
    backgroundSize: 'cover'
	},

	footerItems: {
		display: 'grid',
    gridTemplateColumns: 'repeat(4, 1fr)',
    gridGap: '5rem',
    marginBottom: '2rem'
	},

	footerItemList: {
		fontSize: '1.4rem',
    marginBottom: '1rem',
		color: theme.colors.white,
		':first-child': {
			fontSize: '1.4rem',
			fontWeight: '600'
		},
	},

	footerCopy: {
		display: 'flex',
    justifyContent: 'space-between',
    fontSize: '1.4rem'
	}
});
	
export default Footer;